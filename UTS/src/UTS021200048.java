import java.util.Scanner;
public class UTS021200048  {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String nama, npm, prodi, a, b, c, d, e ;
        double n_tugas, n_uts, n_uas, n_akhir;

        System.out.print("Masukkan nama  : ");
        nama = input.nextLine();
        System.out.print("Masukkan NPM   : ");
        npm = input.nextLine();
        System.out.print("Masukkan Prodi : ");
        prodi = input.nextLine();
        System.out.print("MasukkanNilai Tugas : ");
        n_tugas = input.nextInt();
        System.out.print("Masukkan Nilai UTS  : ");
        n_uts = input.nextInt();
        System.out.print("Masukkan Nilai UAS  : ");
        n_uas = input.nextInt();
        n_akhir = ( 0.40 * n_tugas + 0.25 * n_uts  +  0.35 * n_uas );

        a = ("Lulus dengan ");
        b = ("Lulus dengan ");
        c = ("Lulus dengan ");
        d = ("Tidak Lulus dengan ");
        e = ("Tidak Lulus dengan ");
        System.out.print("\n");
        if(n_akhir >=85 && n_akhir<100){
            System.out.println("Nama  : "+nama);
            System.out.println("NPM   : "+npm);
            System.out.println("Prodi : "+prodi);
            System.out.println(a + "Grade A");
        }else if(n_akhir>=70 && n_akhir<85){
            System.out.println("Nama  : "+nama);
            System.out.println("NPM   : "+npm);
            System.out.println("Prodi : "+prodi);
            System.out.println(b +"Grade B");
        }else if(n_akhir>=60 && n_akhir<70){
            System.out.println("Nama  : "+nama);
            System.out.println("NPM   : "+npm);
            System.out.println("Prodi : "+prodi);
            System.out.println(c + "Grade C");
        }else if(n_akhir>=50 && n_akhir<60){
            System.out.println("Nama  : "+nama);
            System.out.println("NPM   : "+npm);
            System.out.println("Prodi : "+prodi);
            System.out.println(d + "Grade D");

        }else{
            System.out.println("Nama  : "+nama);
            System.out.println("NPM   : "+npm);
            System.out.println("Prodi : "+prodi);
            System.out.println(e + "Grade E");
        }}}
